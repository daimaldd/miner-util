package pkg

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/go-lark/lark"
)

func reboot() {

	//larkNotify("start to reboot")
	//var err error
	//err = syscall.Reboot(syscall.LINUX_REBOOT_CMD_POWER_OFF)
	//if err != nil {
	//	log.Printf("power off failed: %v", err)
	//	larkNotify("failed to reboot, error:" + err.Error())
	//}
	//err = syscall.Reboot(syscall.LINUX_REBOOT_CMD_HALT)
	//if err != nil {
	//	log.Printf("halt failed: %v", err)
	//	larkNotify("failed to reboot, error:" + err.Error())
	//	return
	//}
}

func curlGetCheck(url string) bool {
	_, err := http.Get(url)
	if err != nil {
		log.Println("failed to check, error=" + err.Error())
		return false
	}
	return true
}

func LarkNotify(webhook, txt string) {
	hostname, _ := os.Hostname()
	date := time.Now().Local().Format(time.RFC3339)
	txt = fmt.Sprintf("%s> hostname: %s, txt: %s", date, hostname, txt)
	lark.NewNotificationBot(webhook).PostNotificationV2(lark.OutcomingMessage{MsgType: lark.MsgText, Content: lark.MessageContent{Text: &lark.TextContent{
		Text: txt,
	}}})
}
