use std::time::{SystemTime, Duration};
use clap::{Arg};

fn main () {
    println!("hello word");

    let cmd = clap::App::new("miner-bot")
        .version("1.0")
        .author("Rg")
        .subcommand(
            clap::Command::new("start")
            .arg(Arg::new("webhook").short('W').default_value("").takes_value(true))
            .arg(Arg::new("interval").short('I').default_value("120").takes_value(true)) 
            .arg(Arg::new("port").short('P').default_value("8113").takes_value(true)) 
            ) ;
    let matches = cmd.get_matches();

    match matches.subcommand() {
        Some(("start", matches)) => {
            println!("start run!!!");
            let webhook = matches.get_one::<String>("webhook").unwrap();
            let interval = matches.get_one::<String>("interval").unwrap().parse::<u64>().unwrap();
            let address = matches.get_one::<String>("port").unwrap();
            if !lock(&format!("127.0.0.1:{}", address)) {
                println!("miner-bot has run");
                return;
            }
            start_run(webhook, Duration::from_secs(interval));
            println!("exit program");
        },
        _ => unreachable!("clap should ensure we don't get here"),
    }
}

fn start_run(webhook: &str, interval: Duration) {
   let mut t = SystemTime::now();
    loop {
        println!("check point");
      if check_network() {
         t = SystemTime::now();
         send_notify_by_bot(webhook, "check ok");
         std::thread::sleep(interval);
         continue;
      }

      if t.duration_since(SystemTime::now()).unwrap() > Duration::from_secs(10 * 60) {
         send_notify_by_bot(webhook, "ready to restart the machine");
          if let Err(err) =  system_shutdown::reboot() {
              send_notify_by_bot(webhook, format!("failed to restart the machine: {:?}" , err).as_str());
          };
          return;
      }
   }
}

fn check_network() -> bool {
    let ok = ureq::get("http://www.baidu.com").call().is_ok();
    if !ok {
      println!("failed to check network");
    } 
    ok
}

fn lock(addr: &str) -> bool {
   if let Ok(lis) = std::net::TcpListener::bind(addr) {
       std::thread::spawn(move || {
           loop {
               let _ = lis.accept();
           }
       });
       return true
   }
   false
}

fn send_notify_by_bot(webhook: &str, txt: &str) {
    let name = hostname::get().map(|h| {
        let h = h.to_string_lossy(); 
        String::from(h)
    }).unwrap_or("unkown".to_owned());
    let txt = format!("hostname: {}, txt: {}", name, txt);
    let content = serde_json::json!({
        "msg_type": "text",
        "content": {
            "text": txt,
        },
    }).to_string();
    match ureq::get(webhook)
        .set("Content-Type", "Application/Json")
        .send_string(&content) {
        Ok(_) => {
            println!("send notify succeed");
        }
        Err(err) => {
            println!("failed to send notify: {}", err.to_string());
        }
    }
}
