package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"miner-util/pkg"
)

func main() {
	webhook := os.Args[1]
	accessToken := os.Args[2]
	var checkTime = time.Now()
	for {
		if _, ok := hiveOSCheck(accessToken); ok {
			//pkg.LarkNotify(webhook, "check ok: "+s)
			checkTime = time.Now()
		} else {
			pkg.LarkNotify(webhook, "check fail")
			if time.Since(checkTime) > time.Minute*10 {
				actionHiveOS()
			}
		}
		time.Sleep(time.Second * 60)
	}
}

func actionHiveOS() {

}

func actionReboot() {

}

func hiveOSCheck(accessToken string) (string, bool) {
	httpUrl := "https://api2.hiveos.farm/api/v2/farms"
	req, err := http.NewRequest("GET", httpUrl, nil)
	if err != nil {
		log.Println(err)
		return "", false
	}

	req.Header.Add("Authorization", accessToken)
	req.Header.Add("User-Agent", "Go-Client")

	type FarmData struct {
		Id               int     `json:"id"`
		Name             string  `json:"name"`
		Timezone         string  `json:"timezone"`
		Nonfree          bool    `json:"nonfree"`
		TwofaRequired    bool    `json:"twofa_required"`
		Trusted          bool    `json:"trusted"`
		GpuRedTemp       int     `json:"gpu_red_temp"`
		AsicRedTemp      int     `json:"asic_red_temp"`
		GpuRedFan        int     `json:"gpu_red_fan"`
		AsicRedFan       int     `json:"asic_red_fan"`
		GpuRedAsr        int     `json:"gpu_red_asr"`
		AsicRedAsr       int     `json:"asic_red_asr"`
		GpuRedLa         int     `json:"gpu_red_la"`
		AsicRedLa        int     `json:"asic_red_la"`
		GpuRedCpuTemp    int     `json:"gpu_red_cpu_temp"`
		GpuRedMemTemp    int     `json:"gpu_red_mem_temp"`
		AsicRedBoardTemp int     `json:"asic_red_board_temp"`
		AutocreateHash   string  `json:"autocreate_hash"`
		Locked           bool    `json:"locked"`
		PowerPrice       float64 `json:"power_price"`
		TagIds           []int   `json:"tag_ids"`
		AutoTags         bool    `json:"auto_tags"`
		DefaultFs        struct {
			Field1 int `json:"1"`
		} `json:"default_fs"`
		WorkersCount       int `json:"workers_count"`
		RigsCount          int `json:"rigs_count"`
		AsicsCount         int `json:"asics_count"`
		DisabledRigsCount  int `json:"disabled_rigs_count"`
		DisabledAsicsCount int `json:"disabled_asics_count"`
		Owner              struct {
			Id    int    `json:"id"`
			Login string `json:"login"`
			Name  string `json:"name"`
			Me    bool   `json:"me"`
		} `json:"owner"`
		Money struct {
			IsPaid      bool    `json:"is_paid"`
			IsFree      bool    `json:"is_free"`
			PaidCause   string  `json:"paid_cause"`
			Balance     float32 `json:"balance"`
			Discount    float32 `json:"discount"`
			DailyCost   float32 `json:"daily_cost"`
			MonthlyCost float32 `json:"monthly_cost"`
			Overdraft   bool    `json:"overdraft"`
			CostDetails []struct {
				Type         int     `json:"type"`
				TypeName     string  `json:"type_name"`
				Amount       float32 `json:"amount"`
				MonthlyPrice float32 `json:"monthly_price"`
				MonthlyCost  float32 `json:"monthly_cost"`
				DailyCost    float32 `json:"daily_cost"`
			} `json:"cost_details"`
			DailyPrice    float32 `json:"daily_price"`
			MonthlyPrice  float32 `json:"monthly_price"`
			DailyUseRigs  float32 `json:"daily_use_rigs"`
			DailyUseAsics float32 `json:"daily_use_asics"`
			PricePerRig   float32 `json:"price_per_rig"`
			PricePerAsic  float32 `json:"price_per_asic"`
		} `json:"money"`
		Stats struct {
			WorkersTotal       int     `json:"workers_total"`
			WorkersOnline      int     `json:"workers_online"`
			WorkersOffline     int     `json:"workers_offline"`
			WorkersOverheated  int     `json:"workers_overheated"`
			WorkersNoTemp      int     `json:"workers_no_temp"`
			WorkersOverloaded  int     `json:"workers_overloaded"`
			WorkersInvalid     int     `json:"workers_invalid"`
			WorkersLowAsr      int     `json:"workers_low_asr"`
			WorkersNoHashrate  int     `json:"workers_no_hashrate"`
			WorkersWithProblem int     `json:"workers_with_problem"`
			RigsTotal          int     `json:"rigs_total"`
			RigsOnline         int     `json:"rigs_online"`
			RigsOffline        int     `json:"rigs_offline"`
			RigsPower          int     `json:"rigs_power"`
			GpusTotal          int     `json:"gpus_total"`
			GpusOnline         int     `json:"gpus_online"`
			GpusOffline        int     `json:"gpus_offline"`
			GpusOverheated     int     `json:"gpus_overheated"`
			GpusNoTemp         int     `json:"gpus_no_temp"`
			AsicsTotal         int     `json:"asics_total"`
			AsicsOnline        int     `json:"asics_online"`
			AsicsOffline       int     `json:"asics_offline"`
			AsicsPower         int     `json:"asics_power"`
			BoardsTotal        int     `json:"boards_total"`
			BoardsOnline       int     `json:"boards_online"`
			BoardsOffline      int     `json:"boards_offline"`
			BoardsOverheated   int     `json:"boards_overheated"`
			BoardsNoTemp       int     `json:"boards_no_temp"`
			CpusOnline         int     `json:"cpus_online"`
			DevicesTotal       int     `json:"devices_total"`
			DevicesOnline      int     `json:"devices_online"`
			DevicesOffline     int     `json:"devices_offline"`
			PowerDraw          int     `json:"power_draw"`
			PowerCost          float64 `json:"power_cost"`
			Asr                float64 `json:"asr"`
		} `json:"stats"`
		Hashrates []struct {
			Algo     string `json:"algo"`
			Hashrate int    `json:"hashrate"`
		} `json:"hashrates"`
		HashratesByCoin []struct {
			Coin     string `json:"coin"`
			Algo     string `json:"algo"`
			Hashrate int    `json:"hashrate"`
		} `json:"hashrates_by_coin"`
		PersonalSettings struct {
			IsFavorite bool `json:"is_favorite"`
		} `json:"personal_settings"`
		ChargeOnPool bool `json:"charge_on_pool"`
	}

	type Data struct {
		Data []FarmData `json:"data"`
	}
	var data Data

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Println(err)
		return "", false
	}

	if err = json.NewDecoder(res.Body).Decode(&data); err != nil {
		log.Println(err)
		return "", false
	}
	if len(data.Data) == 0 {
		log.Println("not data")
		return "", false
	}
	s, _ := json.Marshal(data.Data)
	log.Println(string(s))
	dataFarm := data.Data[0]
	if dataFarm.Stats.WorkersOffline > 0 {
		return "offline", false
	}
	b := len(data.Data[0].Hashrates) > 0 && data.Data[0].Hashrates[0].Hashrate > 4002804
	if b {
		return fmt.Sprint(data.Data[0].Hashrates[0].Hashrate), true
	}
	return "", false
}
