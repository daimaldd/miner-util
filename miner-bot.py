import time
import requests
import os
import json
import socket
import sys
import subprocess


def check_network():
    with requests.get("http://www.baidu.com"):
        print("check ok")
        return True
    return False


def check_miner():
    trex = subprocess.getoutput(["pidof t-rex"])
    nb = subprocess.getoutput(["pidof nbminer"])
    if not trex and not nb:
        return False
    return True


def bot_notify(webhook, txt):
    trex = subprocess.getoutput(["pidof t-rex"])
    nb = subprocess.getoutput(["pidof nbminer"])
    minertx = 't-rex: ' + trex + ", nb: " + nb
    tm = time.asctime(time.localtime(time.time()))
    txt = tm + ">> " + socket.gethostname() + ': ' + txt + ' (' + minertx + ')'
    data = {'msg_type': 'text', 'content': {'text': txt}}
    payload = json.dumps(data)
    with requests.post(webhook, data=payload):
        print('succeed to send notify')


def write_pid():
    pid = os.getpid()
    fp = open('/tmp/miner-bot.pid', 'w')
    fp.write(str(pid))
    fp.flush()
    fp.close()
    print('write pid into file, ', os.getpid())


def write_reboot_record():
    subprocess.getoutput(["echo `date` >> /hive/reboot.log"])


def main(webhook):
    write_pid()
    ticks = time.time()
    while True:
        print('start to check!, tm: ', time.time() - ticks)
        if check_miner():
            ticks = time.time()
            bot_notify(webhook, 'check ok')
        else:
            bot_notify(webhook, 'check failed')
            time.sleep(1)
            continue

        if time.time() - ticks > 360:
            bot_notify(webhook, 'ready to restart')
            subprocess.getoutput()
            os.system("reboot")
        time.sleep(120)


main(sys.argv[1])
