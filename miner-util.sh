#!/bin/bash

# stop if error
set -e

# print detail
set -x

curl -L https://gitlab.com/daimaldd/miner-util/-/raw/main/miner-bot.py -o /tmp/miner-bot.py

apt install -y python3-pip
pip3 install requests

kill `cat /tmp/miner-bot.pid`
